﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Windows.Forms.VisualStyles;

namespace OOP___Tower_defense
{
    public abstract class GameObject
    {
        public Vector2 Position;
        public bool IsStatic;
        public bool IsDead;
        public bool HasAnimation;
        public float AnimationSpeed;
        public string ImagePath;

        protected Image MySprite;
        protected float ScaleFactor;
        protected GameWorld GameWorld;
        protected Rectangle DisplayRectangle;
        protected List<Image> AnimationFrames;
        protected float CurrentFrameIndex;
        protected string SoundPath;


        public GameObject(GameWorld pGameWorld, Rectangle pDisplayRectangle, Vector2 pPosition, Image image, float pScaleFactor)
        {
            Position = pPosition;
            ScaleFactor = pScaleFactor;
            GameWorld = pGameWorld;
            DisplayRectangle = pDisplayRectangle;
            MySprite = image;
        }


        public GameObject(GameWorld pGameWorld, Rectangle pDisplayRectangle, Vector2 pPosition, List<Image> images, float pScaleFactor)
        {
            HasAnimation = true;
            Position = pPosition;
            ScaleFactor = pScaleFactor;
            GameWorld = pGameWorld;
            DisplayRectangle = pDisplayRectangle;
            this.AnimationFrames = images;
            AnimationSpeed = 10;

            MySprite = this.AnimationFrames[0];            
        }

        protected Vector2 GetCenter()
        {
            return Position + new Vector2((MySprite.Width * ScaleFactor) / 2, (MySprite.Height * ScaleFactor) / 2);
        }

        public virtual void Reset()
        {
            IsDead = false;
        }

        public virtual void UpdateAnimation(float deltaTime)
        {
            //Calculates the current Index
            CurrentFrameIndex += deltaTime * AnimationSpeed;
            //checks if we need to reset the animation
            if (CurrentFrameIndex >= AnimationFrames.Count)
            {
                CurrentFrameIndex = 0;
            }
            //changes the sprite
            MySprite = AnimationFrames[(int)CurrentFrameIndex];
        }

        protected bool IsWithinBounds(RectangleF pRectangle)
        {
            if (pRectangle.IntersectsWith(CollisionBox))
            {
                return true;
            }
            return false;
        }

        public virtual void Update(float pDeltaTime)
        {
            CheckCollision();
        }

        private void CheckCollision()
        {
            foreach (GameObject tObject in GameWorld.Objects)
            {
                if (tObject != this)
                {
                    if (IsCollidingWith(tObject))
                    {
                        OnCollision(tObject);
                    }
                }
            }
        }

        public abstract void OnCollision(GameObject pOtherObject);

        public RectangleF CollisionBox
        {
            get
            {
                return new RectangleF(Position.X, Position.Y, MySprite.Width * ScaleFactor, MySprite.Height * ScaleFactor);
            }
        }

        public bool IsCollidingWith(GameObject pOtherObject)
        {
            return CollisionBox.IntersectsWith(pOtherObject.CollisionBox);
        }

        public bool IsCollidingWith(RectangleF pCollisionBox)
        {
            return CollisionBox.IntersectsWith(pCollisionBox);

        }


        protected virtual void PlaySound()
        {
            SoundManager.Play(SoundPath);
        }

        public virtual void Die()
        {
            IsDead = true;
            GameWorld.RemoveObjectInNextCycle(this);
        }

        public virtual void Draw(Graphics pGraphics)
        {
            pGraphics.DrawImage(MySprite, Position.X, Position.Y, MySprite.Width * ScaleFactor, MySprite.Height * ScaleFactor);
        }
    }
}
