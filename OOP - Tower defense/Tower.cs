﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP___Tower_defense
{
    class Tower : GameObject
    {
        public Tower(GameWorld pGameWorld, Rectangle pDisplayRectangle, Vector2 pPosition, Image image, float pScaleFactor, int health, float speed) : base(pGameWorld, pDisplayRectangle, pPosition, image, pScaleFactor)
        {

        }

        public override void OnCollision(GameObject pOtherObject)
        {
            
        }

        public virtual void Attack()
        {

        }
    }
}
