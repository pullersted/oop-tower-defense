﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using EpPathFinding;

namespace OOP___Tower_defense
{
    public class Vector2
    {


        private bool Empty;

        private float x;
        private float y;

        public bool IsEmpty
        {
            get
            {
                return Empty;
            }
            private set
            {
                Empty = value;
            }
        }

        public float X {
            get
            {
                return x; 
            } 
            set
            {
                Empty = false;
                x = value;
            }
        }
        public float Y {
            get
            {
                return y;
            }
            set
            {
                Empty = false;
                y = value;
            }
        }

        public Vector2()
        {
            Empty = true;
        }

        public Vector2(float pX, float pY)
        {
            x = pX;
            y = pY;
        }

        public static Vector2 operator+ (Vector2 pVector1, Vector2 pVector2)
        {
            Vector2 tAdditionVector = new Vector2(0,0);

            tAdditionVector.X = pVector1.X + pVector2.X;
            tAdditionVector.Y = pVector1.Y + pVector2.Y;

            return tAdditionVector;
        }

        public static Vector2 operator -(Vector2 pVector1, Vector2 pVector2)
        {
            Vector2 tSubtractVector = new Vector2(0, 0);

            tSubtractVector.X = pVector1.X - pVector2.X;
            tSubtractVector.Y = pVector1.Y - pVector2.Y;

            return tSubtractVector;
        }

        public static Vector2 operator *(Vector2 pVector1, float pMultiplyFactor)
        {
            Vector2 tMultiplyVector = new Vector2(0, 0);

            tMultiplyVector.X = pVector1.X * pMultiplyFactor;
            tMultiplyVector.Y = pVector1.Y * pMultiplyFactor;

            return tMultiplyVector;
        }

        public static Vector2 operator /(Vector2 pVector1, float pMultiplyFactor)
        {
            Vector2 tDividerVector = new Vector2(0, 0);

            tDividerVector.X = pVector1.X / pMultiplyFactor;
            tDividerVector.Y = pVector1.Y / pMultiplyFactor;

            return tDividerVector;
        }

       

        

        public Point ToPoint()
        {
            return new Point((int)X, (int)Y);
        }
        public PointF ToPointF()
        {
            return new PointF(X, Y);
        }

        public GridPos ToGridPos()
        {
            return new GridPos((int)X, (int)Y);
        }

        public string GetAsString()
        {
            return "(X : " + X + " Y : " + Y + ")";
        }

        public float Length()
        {
            return (float)Math.Sqrt((x * x) + (y * y));
        }

        public Vector2 GetNormalized()
        {
            float tLength = Length();
            return new Vector2(X / tLength, Y / tLength);
        }

        public void Normalize()
        {
            float tLength = Length();

            x = x / tLength;
            y = y / tLength;
        }


        public float ToRadians(float val)
        {
            return (float)(Math.PI / 180) * val;
        }

        public float ToDegrees(float val)
        {
            return (float)(180.0 / Math.PI) * val;
        }

        public Vector2 GetDirectionBasedOnAngle(float pAngleInDegrees)
        {
            float tAngleInRadians = ToRadians(pAngleInDegrees);

            return new Vector2((float)Math.Cos(tAngleInRadians), (float)Math.Sin(tAngleInRadians));
        }

        public float GetAngleBasedOnDirection()
        {
            return ToDegrees((float)(Math.Atan2(x, -y)));
        }
    }
}
