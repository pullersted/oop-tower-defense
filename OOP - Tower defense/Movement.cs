﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace OOP___Tower_defense
{
    class Movement
    {
        public float Speed;

        private float mReachedWaypointDistance = 5;
        private bool UsePathfinding;

        private float MyStartSpeed;
        private GameObject mObjectToMove;
        private Vector2 mTargetPosition;
        private Queue<Vector2> mPathToTarget = new Queue<Vector2>();
        private Vector2 mCurrentWaypoint;
        private PathFinder mPathFinder;

        private bool mIsMoving;

        public bool IsMoving
        {
            get
            {
                return mIsMoving;
            }
            private set
            {
                mIsMoving = value;
            }
        }

        public float StartSpeed
        {
            get
            {
                return MyStartSpeed;
            }
            private set
            {
                MyStartSpeed = value;
            }
        }


        public Movement(GameObject pObjectToMove,PathFinder pPathFinder,Vector2 pTargetPosition,float pSpeed,bool usePathfinding)
        {
            mObjectToMove = pObjectToMove;
            mTargetPosition = pTargetPosition;
            mPathFinder = pPathFinder;
            Speed = pSpeed;
            MyStartSpeed = Speed;
            SetTarget(pTargetPosition,usePathfinding);
        }

        public void SetTarget(Vector2 pTargetPosition,bool usePathfinding)
        {
            mIsMoving = true;
            UsePathfinding = usePathfinding;
            mTargetPosition = pTargetPosition;
            if(usePathfinding)
            {
                mPathFinder.SetupGrid(new Vector2(mObjectToMove.CollisionBox.Width, mObjectToMove.CollisionBox.Height));
                mPathToTarget = mPathFinder.GetPath(mObjectToMove.Position, pTargetPosition);
                if (mPathToTarget.Count > 0)
                {
                    mCurrentWaypoint = mPathToTarget.Dequeue();
                }
            }
            else
            {
                mCurrentWaypoint = mTargetPosition;
            }

           
            
        }

        public void Update(float pDeltaTime)
        {

            if (mPathToTarget.Count <= 0 && HasReachedWaypoint() && UsePathfinding)
            {
                mIsMoving = false;
                return;
            }         

            while(UsePathfinding && (mCurrentWaypoint == null || HasReachedWaypoint()) && mPathToTarget.Count > 0)
            {
                mCurrentWaypoint = mPathToTarget.Dequeue();
                break;
            }
            if(mCurrentWaypoint != null)
            {
                Vector2 tDirection = (mCurrentWaypoint - mObjectToMove.Position);
                tDirection.Normalize();
                mObjectToMove.Position += tDirection * pDeltaTime * Speed;

            }

        }

        bool HasReachedWaypoint()
        {
            if(mCurrentWaypoint == null)
            {
                return false;
            }

            return (mCurrentWaypoint - mObjectToMove.Position).Length() < mReachedWaypointDistance;
        }

    }
}
