﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;

namespace OOP___Tower_defense
{
    public partial class Form1 : Form
    {
        public static Vector2 TheMousePosition = new Vector2();
        public static bool ClickedOnScreen;
        public static Form1 Self;

        private Graphics mGraphics;

        private GameWorld mGameWorld;
        private MouseButtons LastMouseButtons;
        private Menu mMenu;

        public Form1()
        {            
            InitializeComponent();
            Self = this;
            Init();
            /*Track Name:"Orchestral >> Dragon Warrior (Loopable).Wav "
            Composed by: Marcus Dellicompagni 
            Website: www.PoundSound.co.uk
            http://freesound.org/people/dingo1/sounds/243979/            
            */
            SoundManager.Play(@"Sounds\243979__dingo1__dragon-warrior-loopable.wav", true);
        }

        public void Init()
        {
            if (mGraphics == null)
            {
                mGraphics = CreateGraphics();
            }

            mGameWorld = new GameWorld(mGraphics, DisplayRectangle);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            GameManager.IsActive = true;
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            if(GameManager.IsActive)
            {
                if (MouseButtons != MouseButtons.None && LastMouseButtons != MouseButtons)
                {
                    ClickedOnScreen = true;
                }
                else
                {
                    ClickedOnScreen = false;
                }
                mGameWorld.GameLoop();
                TheMousePosition = new Vector2(this.PointToClient(Control.MousePosition).X, this.PointToClient(Control.MousePosition).Y);

                LastMouseButtons = MouseButtons;
            }
       
        }

        void Test()
        {

        }

        private void BuyTower_Click(object sender, EventArgs e)
        {
           
        }

        private void BuyTowerDown(object sender, MouseEventArgs e)
        {           

           

        }
    }
}
