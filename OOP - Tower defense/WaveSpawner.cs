﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Diagnostics;

namespace OOP___Tower_defense
{
    public enum EnemyType
    {
        Standard,
        Flying,
        Heavy,
        Spawner,
        Fast
    }

    class WaveSpawner
    {
        private GameWorld GameWorld;
        private Rectangle DisplayRectangle;
        private PathFinder PathFinder;

        private float SpawnTimer;
        private bool DoneSpawning;

        private Queue<WaveInfo> SpawninigQueue = new Queue<WaveInfo>();

        public bool IsDoneSpawning
        {
            get
            {
                return DoneSpawning;
            }
            private set
            {
                DoneSpawning = value;
            }
        }

        public WaveSpawner(GameWorld gameWorld, Rectangle displayRectangle,PathFinder pathFinder)
        {                 
            GameWorld = gameWorld;
            DisplayRectangle = displayRectangle;
            PathFinder = pathFinder;
        }

        public void Spawn(List<WaveInfo> whatToSpawn)
        {
            DoneSpawning = false;
            foreach(WaveInfo tWaveInfo in whatToSpawn)
            {
                for(int tSpawnNumber = 0;tSpawnNumber < tWaveInfo.SpawnNumber;tSpawnNumber++)
                {
                    SpawninigQueue.Enqueue(tWaveInfo);
                }

            }           
             
        }

        void Spawn(WaveInfo pWaveInfo)
        {
            Enemy tEnemy;
            switch (pWaveInfo.EnemyType)
            {
                case EnemyType.Standard:                      
                        tEnemy = new EnemyStandard(GameWorld, DisplayRectangle, pWaveInfo.SpawnPosition, pWaveInfo.Image, pWaveInfo.ScaleFactor,PathFinder);                        
                        GameWorld.AddObjectToNextCycle(tEnemy);                                 
                    break;
                case EnemyType.Fast:
                        tEnemy = new EnemyFast(GameWorld, DisplayRectangle, pWaveInfo.SpawnPosition, pWaveInfo.Image, pWaveInfo.ScaleFactor, PathFinder);
                        GameWorld.AddObjectToNextCycle(tEnemy);
                    break;
                case EnemyType.Flying:
                        tEnemy = new EnemyFlying(GameWorld, DisplayRectangle, pWaveInfo.SpawnPosition, pWaveInfo.Image, pWaveInfo.ScaleFactor, PathFinder);
                        GameWorld.AddObjectToNextCycle(tEnemy);
                    break;
                case EnemyType.Heavy:
                        tEnemy = new EnemyHeavy(GameWorld, DisplayRectangle, pWaveInfo.SpawnPosition, pWaveInfo.Image, pWaveInfo.ScaleFactor, PathFinder);
                        GameWorld.AddObjectToNextCycle(tEnemy);
                    break;
                case EnemyType.Spawner:
                        tEnemy = new EnemySpawner(GameWorld, DisplayRectangle, pWaveInfo.SpawnPosition, pWaveInfo.Image, pWaveInfo.ScaleFactor, PathFinder);
                        GameWorld.AddObjectToNextCycle(tEnemy);
                    break;
            }
        }

        public  void Update(float deltaTime)
        {
            if(SpawnTimer > 0)
            {
                SpawnTimer -= deltaTime;
            }
            if(SpawnTimer <= 0 && SpawninigQueue.Count > 0)
            {
                WaveInfo tWaveInfo = SpawninigQueue.Dequeue();
                Spawn(tWaveInfo);
                SpawnTimer = tWaveInfo.SpawnWaitInterval;
            }
            if(SpawninigQueue.Count <= 0)
            {
                DoneSpawning = true;
            }
        }
    }
}
