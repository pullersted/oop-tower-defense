﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Diagnostics;

namespace OOP___Tower_defense
{
    class Slow : GameObject
    {
        private float SlowDuration;
        private float SlowProcent;

        private Movement Target;

        public Slow(GameWorld pGameWorld, Rectangle pDisplayRectangle, Vector2 pPosition, Image pImageUrl, float pScaleFactor, Movement target, float slowProcent, float slowDuration) :
            base(pGameWorld, pDisplayRectangle, pPosition, pImageUrl, pScaleFactor)
        {
            if (target == null)
            {
                Die();
            }
            else
            {
                Target = target;
                SlowProcent = slowProcent;
                SlowDuration = slowDuration;

                SlowTarget();
                GameWorld.AddObjectToNextCycle(new DestroyTarget(GameWorld, DisplayRectangle, Position, MySprite, 0, slowDuration, this));
            }           
        }

        public override void Die()
        {
            UnSlow();
            base.Die();
        }

        void UnSlow()
        {
            Target.Speed = Target.StartSpeed;
        }

        void SlowTarget()
        {
            Target.Speed = Target.StartSpeed * (1 - SlowProcent);
        }

        public override void OnCollision(GameObject pOtherObject) { }
        public override void Draw(Graphics pGraphics) { }

    }
}
