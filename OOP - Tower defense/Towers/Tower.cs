﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using MoreLinq;

namespace OOP___Tower_defense
{

    abstract class Tower : GameObject
    {
        public Circle AttackRangeCollisionCircle;
        public TowerStat TowerStat;
        public int SellMoney = 50;
        public bool DrawAttackRange;

        protected bool CanShootFlying;
        protected List<Enemy> EnemiesInAttackRange;
        protected Image ShootImage;
        protected float SellProcent = 0.75f;

        protected float AttackTimer;

        protected Queue<TowerStat> TowerUpgradeStats = new Queue<TowerStat>();

        private TowerInfo TowerUpgradeInfo;
        private TowerButton TowerButton;
        private TowerStat LastTowerStat;
        private float Cost;



        public Tower(GameWorld pGameWorld, Rectangle pDisplayRectangle, Vector2 pPosition, Image pImageUrl, float pScaleFactor,Image shootImg) : base(pGameWorld, pDisplayRectangle, pPosition, pImageUrl, pScaleFactor)
        {
            TowerStat = new TowerStat(75, 5, 2);
            ShootImage = shootImg;
            SoundPath = @"Sounds\machine one shot.wav";
            UpgradeStats();
        }

        protected virtual void UpgradeStats()
        {
            TowerUpgradeStats.Enqueue(new TowerStat(10, 5, 0.1f,30,"Test"));
            TowerUpgradeStats.Enqueue(new TowerStat(15, 10, 0.2f,40, "Test"));
            TowerUpgradeStats.Enqueue(new TowerStat(20, 15, 0.4f,50, "Test"));
            TowerUpgradeStats.Enqueue(new TowerStat(25, 20, 0.5f,60, "Test"));
        }

        public void Init(float cost)
        {
            TowerStat.Cost = (int)cost;
            SellMoney = (int)(cost * SellProcent);
            TowerUpgradeInfo = new TowerInfo(this, TowerUpgradeStats);
            TowerButton = new TowerButton(GameWorld, DisplayRectangle, Position, MySprite, ScaleFactor, "", TowerUpgradeInfo);
            GameWorld.AddObjectToNextCycle(TowerButton);
            UpdateStats();
        }

        public override void Update(float pDeltaTime)
        {
            if(StatsChanged())
            {
                UpdateStats();
            }
            SetEnemiesInAttackRange();
            AttackTimer -= pDeltaTime;
            CheckAttackRange();
            
            LastTowerStat = TowerStat;
        }

        void SetEnemiesInAttackRange()
        {
            EnemiesInAttackRange = GameWorld.Objects.FindAll(item => item is Enemy && AttackRangeCollisionCircle.IntersectsWith(item.CollisionBox)).Cast<Enemy>().ToList();
            EnemiesInAttackRange = EnemiesInAttackRange.FindAll(item => (item.IsFlying && CanShootFlying) || !item.IsFlying);
        }

        bool StatsChanged()
        {
            return LastTowerStat != TowerStat;
        }


        void UpdateStats()
        {
            AttackRangeCollisionCircle = new Circle(Position + new Vector2(CollisionBox.Width / 2, CollisionBox.Height / 2), TowerStat.AttackRange);
            SellMoney = (int)((float)TowerStat.Cost * SellProcent);
        }

        Enemy GetClosestEnemy()
        {
            if(EnemiesInAttackRange.Count > 0)
            {
                return EnemiesInAttackRange.MinBy(item => (item.Position - Position).Length());
            }
            return null;
            
        }


        protected virtual void CheckAttackRange()
        {
            Enemy enemyToShoot = GetClosestEnemy();

            if (enemyToShoot != null && AttackTimer <= 0)
            {
                Shoot(enemyToShoot);
                AttackTimer = TowerStat.AttackSpeed;
            }
        }

        public override void Draw(Graphics pGraphics)
        {
            pGraphics.DrawImage(MySprite, Position.X, Position.Y, MySprite.Width * ScaleFactor, MySprite.Height * ScaleFactor);
            if(DrawAttackRange)
            {
                Vector2 CenterPosition = Position - new Vector2(TowerStat.AttackRange - CollisionBox.Width / 2, TowerStat.AttackRange - CollisionBox.Height / 2);
                pGraphics.DrawEllipse(new Pen(Color.Red), new RectangleF(CenterPosition.X, CenterPosition.Y, TowerStat.AttackRange * 2, TowerStat.AttackRange * 2));
            }
           
        }

        public override void OnCollision(GameObject pOtherObject)
        {
            throw new NotImplementedException();
        }
        


        public virtual void Shoot(Enemy enemyToShoot)
        {
            PlaySound();
        }
    }
}
