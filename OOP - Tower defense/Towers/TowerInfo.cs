﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace OOP___Tower_defense
{
    struct TowerInfo
    {
        public Tower Tower;
        public Queue<TowerStat> TowerUpgradeStats;

        public TowerInfo(Tower towerToUpgrade, Queue<TowerStat> towerUpgradeStats)
        {
            Tower = towerToUpgrade;
            TowerUpgradeStats = towerUpgradeStats;
        }
    }
}
