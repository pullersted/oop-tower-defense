﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP___Tower_defense
{
    class TowerFast : Tower
    {
        public TowerFast(GameWorld pGameWorld, Rectangle pDisplayRectangle, Vector2 pPosition, Image pImageUrl, float pScaleFactor,Image shootImage) : 
            base(pGameWorld, pDisplayRectangle, pPosition, pImageUrl, pScaleFactor,shootImage)
        {
            TowerStat = new TowerStat();
            TowerStat.AttackSpeed = 0.25f;
            TowerStat.AttackRange = 50;
            TowerStat.Damage = 0.75f;
            CanShootFlying = true;
            
        }

        public override void Shoot(Enemy enemyToShoot)
        {
            base.Shoot(enemyToShoot);
            GameWorld.AddObjectToNextCycle(new Fast_bullet(GameWorld, DisplayRectangle, GetCenter(), ShootImage, 0.5f, enemyToShoot, TowerStat.Damage));
        }

        protected override void UpgradeStats()
        {
            TowerUpgradeStats.Enqueue(new TowerStat(3, 0,0.1f, 50, "More range and attacks quicker"));
            TowerUpgradeStats.Enqueue(new TowerStat(3, 0, 0.05f, 90, "More range and attacks quicker"));
            TowerUpgradeStats.Enqueue(new TowerStat(3, 0.3f, 0, 170, "More range and dmg"));
            TowerUpgradeStats.Enqueue(new TowerStat(3, 0.45f, 0, 200, "More range and dmg"));
            TowerUpgradeStats.Enqueue(new TowerStat(3, 0.55f, 0, 240, "More range and dmg"));

        }

    }
}
