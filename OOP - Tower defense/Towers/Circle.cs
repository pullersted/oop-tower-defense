﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace OOP___Tower_defense
{
     class Circle
     {
        private float MyRadius;
        private Vector2 MyCenter;
        public float Radius
        {
            get { return MyRadius; }
            private set { MyRadius = value; }
        }

        public Vector2 Center
        {
            get { return MyCenter; }
            private set { MyCenter = value; }
        }

        public Circle(Vector2 center, float radius)
        {
            Center = center;
            Radius = radius;
        }

        public float Clamp(float value, float min, float max)
        {
            return (value < min) ? min : (value > max) ? max : value;
        }

        //http://stackoverflow.com/questions/401847/circle-rectangle-collision-detection-intersection/1879223#1879223
        public bool IntersectsWith(RectangleF other)
         {
            // Find the closest point to the circle within the rectangle
            float closestX = Clamp(Center.X, other.Left, other.Right);
            float closestY = Clamp(Center.Y, other.Top, other.Bottom);

            // Calculate the distance between the circle's center and this closest point
            float distanceX = Center.X - closestX;
            float distanceY = Center.Y - closestY;

            // If the distance is less than the circle's radius, an intersection occurs
            float distanceSquared = (distanceX * distanceX) + (distanceY * distanceY);
            return distanceSquared< (Radius* Radius);
         }
       
    }
}
