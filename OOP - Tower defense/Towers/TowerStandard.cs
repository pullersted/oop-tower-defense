﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP___Tower_defense
{
    class TowerStandard : Tower
    {
        public TowerStandard(GameWorld pGameWorld, Rectangle pDisplayRectangle, Vector2 pPosition, Image pImageUrl, float pScaleFactor, Image shootImage) : 
            base(pGameWorld, pDisplayRectangle, pPosition, pImageUrl, pScaleFactor,shootImage)
        {
            TowerStat = new TowerStat();
            TowerStat.AttackSpeed = 1f;
            TowerStat.Damage = 3f;
            TowerStat.AttackRange = 75;
            CanShootFlying = true;
        }

        public override void Shoot(Enemy enemyToShoot)
        {
            base.Shoot(enemyToShoot);
            GameWorld.AddObjectToNextCycle(new Standard_gun_bullet(GameWorld, DisplayRectangle, GetCenter(), ShootImage, 0.5f, enemyToShoot, TowerStat.Damage));
        }

        protected override void UpgradeStats()
        {
            TowerUpgradeStats.Enqueue(new TowerStat(0, 1.75f, 0.05f, 50, "More dmg and attacks quicker"));
            TowerUpgradeStats.Enqueue(new TowerStat(0, 1.75f, 0.05f, 100, "More dmg and attacks quicker"));
            TowerUpgradeStats.Enqueue(new TowerStat(0, 1.75f, 0.05f, 100, "More dmg and attacks quicker"));
            TowerUpgradeStats.Enqueue(new TowerStat(0, 1.75f, 0.05f, 100, "More dmg and attacks quicker"));
            TowerUpgradeStats.Enqueue(new TowerStat(0, 1.75f, 0.05f, 150, "More dmg and attacks quicker"));

        }
    }
}
