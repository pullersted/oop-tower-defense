﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP___Tower_defense
{
    class TowerHeavy : Tower
    {
        public TowerHeavy(GameWorld pGameWorld, Rectangle pDisplayRectangle, Vector2 pPosition, Image pImageUrl, float pScaleFactor, Image shootImage) : 
            base(pGameWorld, pDisplayRectangle, pPosition, pImageUrl, pScaleFactor,shootImage)
        {
            TowerStat = new TowerStat();
            TowerStat.AttackSpeed = 2.5f;
            TowerStat.Damage = 4;
            TowerStat.AttackRange = 100;
            CanShootFlying = true;
            SoundPath = @"Sounds\755__elmomo__missile01.wav";
        }

        public override void Shoot(Enemy enemyToShoot)
        {
            base.Shoot(enemyToShoot);
            GameWorld.AddObjectToNextCycle(new Rocket_attack(GameWorld, DisplayRectangle, GetCenter(), ShootImage, 0.5f, enemyToShoot, TowerStat.Damage));
        }

        protected override void UpgradeStats()
        {
            TowerUpgradeStats.Enqueue(new TowerStat(0, 0.8f,0.1f,150,"More dmg and attacks quicker"));
            TowerUpgradeStats.Enqueue(new TowerStat(0, 0.8f, 0.1f, 150, "More dmg and attacks quicker"));
            TowerUpgradeStats.Enqueue(new TowerStat(0, 0.8f, 0.1f, 150, "More dmg and attacks quicker"));
            TowerUpgradeStats.Enqueue(new TowerStat(0, 0.8f, 0.1f, 150, "More dmg and attacks quicker"));
            TowerUpgradeStats.Enqueue(new TowerStat(0, 0.8f, 0.1f, 150, "More dmg and attacks quicker"));


        }
    }
}
