﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP___Tower_defense
{
    class TowerPoison : Tower
    {
        public TowerPoison(GameWorld pGameWorld, Rectangle pDisplayRectangle, Vector2 pPosition, Image pImageUrl, float pScaleFactor, Image shootImage) : 
            base(pGameWorld, pDisplayRectangle, pPosition, pImageUrl, pScaleFactor,shootImage)
        {
            TowerStat = new TowerStat();
            TowerStat.AttackSpeed = 1f;
            TowerStat.AttackRange = 60;
            TowerStat.Damage = 3;
        }

        public override void Shoot(Enemy enemyToShoot)
        {
            base.Shoot(enemyToShoot);
            GameWorld.AddObjectToNextCycle(new poison_slow(GameWorld, DisplayRectangle, GetCenter(), ShootImage, 0.5f, enemyToShoot, TowerStat.Damage));
        }

        protected override void UpgradeStats()
        {
            TowerUpgradeStats.Enqueue(new TowerStat(0,0,0.15f,125, "Attacks quicker"));
            TowerUpgradeStats.Enqueue(new TowerStat(0, 0, 0.15f, 125, "Attacks quicker"));
            TowerUpgradeStats.Enqueue(new TowerStat(0, 0, 0.15f, 125, "Attacks quicker"));
            TowerUpgradeStats.Enqueue(new TowerStat(0, 0, 0.15f, 125, "Attacks quicker"));
            TowerUpgradeStats.Enqueue(new TowerStat(0, 0, 0.15f, 125, "Attacks quicker"));


        }
    }
}
