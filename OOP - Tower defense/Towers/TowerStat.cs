﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP___Tower_defense
{
    class TowerStat
    {
        private float MyDamage;
        private int MyAttackRange;
        private int MyCost;

        private float MyAttackSpeed;

        private string MyDescription;
        
        public int Cost
        {
            get { return MyCost; }
            set
            {
                if (value >= 0)
                {
                    MyCost = value;
                }
            }
        }

        public int AttackRange
        {
            get { return MyAttackRange;  }
            set
            {
                if (value >= 0)
                {
                    MyAttackRange = value;
                }
            }
        }

        public float Damage
        {
            get { return MyDamage; }
            set
            {
                if (value >= 0)
                {
                    MyDamage = value;
                }
            }
        }

        public float AttackSpeed
        {
            get  { return MyAttackSpeed;  }
            set
            {
                if(value >= 0)
                {
                    MyAttackSpeed = value;
                }
            }
        }

        public string Description
        {
            get { return MyDescription; }
            private set{ MyDescription = value;  }
        }

        public TowerStat()
        {

        }

        public TowerStat(int attackRange,float damage,float attackSpeed)
        {
            MyAttackRange = attackRange;
            MyDamage = damage;
            MyAttackSpeed = attackSpeed;
        }

        public TowerStat(int attackRange, float damage, float attackSpeed,int cost,string description)
        {
            Description = description;
            MyAttackRange = attackRange;
            MyDamage = damage;
            MyAttackSpeed = attackSpeed;
            Cost = cost;
        }

        public static TowerStat operator +(TowerStat towerStats1, TowerStat towerStats2)
        {
            TowerStat NewTowerStats = new TowerStat();

            NewTowerStats.AttackRange = towerStats1.AttackRange + towerStats2.AttackRange;
            NewTowerStats.AttackSpeed = towerStats1.AttackSpeed - towerStats2.AttackSpeed;
            NewTowerStats.Damage = towerStats1.Damage + towerStats2.Damage;
            NewTowerStats.Cost = towerStats1.Cost + towerStats2.Cost;
            NewTowerStats.Description = towerStats2.Description;

            return NewTowerStats;
        }

        public static TowerStat operator -(TowerStat towerStats1, TowerStat towerStats2)
        {
            TowerStat NewTowerStats = new TowerStat();

            NewTowerStats.AttackRange = towerStats1.AttackRange - towerStats2.AttackRange;
            NewTowerStats.AttackSpeed = towerStats1.AttackSpeed + towerStats2.AttackSpeed;
            NewTowerStats.Damage = towerStats1.Damage - towerStats2.Damage;
            NewTowerStats.Damage = towerStats1.Cost - towerStats2.Cost;

            return NewTowerStats;
        }



    }
}
