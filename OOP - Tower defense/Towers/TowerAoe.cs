﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP___Tower_defense
{
    class TowerAoe : Tower
    {
        public TowerAoe(GameWorld pGameWorld, Rectangle pDisplayRectangle, Vector2 pPosition, Image pImageUrl, float pScaleFactor,Image shootImage) : 
            base(pGameWorld, pDisplayRectangle, pPosition, pImageUrl, pScaleFactor, shootImage)
        {
            TowerStat = new TowerStat();
            TowerStat.AttackSpeed = 1.5f;
            TowerStat.AttackRange = 50;
            TowerStat.Damage = 2;
            SoundPath = @"Sounds\tesla.wav";

        }

        protected override void CheckAttackRange()
        {
            if(AttackTimer <= 0)
            {              
                foreach (Enemy EnemyToShoot in EnemiesInAttackRange)
                {
                    if (EnemyToShoot != null)
                    {
                        Shoot(EnemyToShoot);                        
                    }
                }
                if(EnemiesInAttackRange.Count > 0)
                {
                    PlaySound();
                    AttackTimer = TowerStat.AttackSpeed;
                }                
            }
            
            
        }

        protected override void UpgradeStats()
        {
            TowerUpgradeStats.Enqueue(new TowerStat(3, 0.6f,0.1f, 200, "More range, dmg and attacks quicker"));
            TowerUpgradeStats.Enqueue(new TowerStat(3, 0.6f, 0.1f, 200, "More range, dmg and attacks quicker"));
            TowerUpgradeStats.Enqueue(new TowerStat(3, 0.6f, 0.1f, 200, "More range, dmg and attacks quicker"));
            TowerUpgradeStats.Enqueue(new TowerStat(3, 0.6f, 0.1f, 200, "More range, dmg and attacks quicker"));
            TowerUpgradeStats.Enqueue(new TowerStat(3, 0.6f, 0.1f, 200, "More range, dmg and attacks quicker"));

        }

        public override void Shoot(Enemy enemyToShoot)
        {
            GameWorld.AddObjectToNextCycle(new Lightning_attack(GameWorld, DisplayRectangle, GetCenter(), ShootImage, 0.5f, enemyToShoot, TowerStat.Damage));
        }
    }
}
