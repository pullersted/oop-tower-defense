﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EpPathFinding;
using System.Drawing;
using System.Diagnostics;

namespace OOP___Tower_defense
{
    public class PathFinder
    {
        private BaseGrid mSearchGrid;
        private Vector2 mSize;
        private GameWorld mGameWorld;
        private float mGridIncrementer;
        private JumpPointParam mJumpPointParam;

        public Vector2 Size
        {
            get
            {
                return mSize;
            }
            private set
            {
                mSize = value;
            } 
        }

        public float GridIncrementer
        {
            get
            {
                return mGridIncrementer;
            }
            private set
            {
                mGridIncrementer = value;
            }
        }

        public PathFinder(GameWorld pGameWorld,Vector2 pSize,float pGridIncrementer)
        {
            mSize = pSize;
            mGameWorld = pGameWorld;
            mGridIncrementer = pGridIncrementer;
        }


        public void SetupGrid(Vector2 pSizeOfObject)
        {
            bool[][] tWalkableMatrix = new bool[(int)mSize.X][];

            for (int tX = 0;tX < mSize.X;tX++)
            {
                tWalkableMatrix[tX] = new bool[(int)mSize.Y];
                for (int tY = 0; tY < mSize.Y; tY++)
                {
                    if(!mGameWorld.StaticCollision.Exists(item => 
                    item.IntersectsWith(new RectangleF(tX * mGridIncrementer, tY * mGridIncrementer, pSizeOfObject.X, pSizeOfObject.Y))))
                    {                     
                        tWalkableMatrix[tX][tY] = true;
                    }
                }
            }

            mSearchGrid = new StaticGrid((int)mSize.X,(int) mSize.Y, tWalkableMatrix);
            mJumpPointParam = new JumpPointParam(mSearchGrid, true, false, false, HeuristicMode.EUCLIDEAN);
        }

        public Queue<Vector2> GetPath(Vector2 pStartPosition,Vector2 pEndPosition)
        {            
            mJumpPointParam.Reset((pStartPosition/mGridIncrementer).ToGridPos(), (pEndPosition/mGridIncrementer).ToGridPos());

            Queue<Vector2> tPath = new Queue<Vector2>();

            List<GridPos> tAPath = JumpPointFinder.FindPath(mJumpPointParam);
            bool tFirst = true;
            foreach (GridPos tGridPos in tAPath)
            {
                if(!tFirst)
                {
                    tPath.Enqueue(new Vector2(tGridPos.x * mGridIncrementer, tGridPos.y * mGridIncrementer));
                }
                else
                {
                    tFirst = false;
                }
               
            }
           
            return tPath;

        }
    }
}
