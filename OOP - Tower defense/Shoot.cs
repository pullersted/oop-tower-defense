﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP___Tower_defense
{
    internal abstract class Shoot : GameObject
    {
        protected float Damage;
        protected float ProjectileSpeed;
        protected Enemy TargetEnemy;

        public Shoot(GameWorld pGameWorld, Rectangle pDisplayRectangle, Vector2 pPosition, Image pImageUrl, float pScaleFactor, Enemy enemy,float damage) : 
            base(pGameWorld, pDisplayRectangle, pPosition, pImageUrl, pScaleFactor)
        {
            TargetEnemy = enemy;
            Damage = damage;
        }

        public override void Update(float pDeltaTime)
        {
            base.Update(pDeltaTime);
            CheckDeathConditions();
            Move(pDeltaTime);
        }

        private void Move(float deltaTime)
        {
            Vector2 Direction = (TargetEnemy.Position - Position);
            Direction.Normalize();
            Position += Direction * ProjectileSpeed * deltaTime;
        }

        void CheckDeathConditions()
        {
            if(TargetEnemy == null || TargetEnemy.Health <= 0)
            {
                Die();
            }
        }

        protected virtual void DamageEnemy(Enemy enemyToDamage)
        {
            enemyToDamage.Health -= Damage;
        }

        protected virtual void CollidingWithEnemy(Enemy collidingEnemy)
        {
            DamageEnemy(collidingEnemy);
            Die();
        }

        public override void OnCollision(GameObject otherObject)
        {
            if (otherObject == TargetEnemy)
            {
                CollidingWithEnemy(TargetEnemy);
            }
        }
    }
}