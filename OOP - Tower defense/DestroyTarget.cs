﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace OOP___Tower_defense
{
    class DestroyTarget : GameObject
    {
        private float DeathTimer;
        private GameObject ObjectToDestroy;

        public DestroyTarget(GameWorld pGameWorld, Rectangle pDisplayRectangle, Vector2 pPosition, Image pImageUrl, float pScaleFactor,float deathTimer,GameObject objectToDestroy) : 
            base(pGameWorld, pDisplayRectangle, pPosition, pImageUrl, pScaleFactor)
        {
            DeathTimer = deathTimer;
            ObjectToDestroy = objectToDestroy;
        }

        public override void Update(float deltaTime)
        {
            DeathTimer -= deltaTime;
            if(DeathTimer <= 0)
            {
                ObjectToDestroy.Die();
                Die();
            }
        }

        public override void Draw(Graphics pGraphics) { }

        public override void OnCollision(GameObject pOtherObject) { }
    }
}
