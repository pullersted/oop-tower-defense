﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace OOP___Tower_defense
{
    class StaticObject : GameObject
    {
        public StaticObject(GameWorld pGameWorld, Rectangle pDisplayRectangle, Vector2 pPosition, Image image, float pScaleFactor) :
            base(pGameWorld,pDisplayRectangle,pPosition, image, pScaleFactor)
        {
            IsStatic = true;
        }

        public override void OnCollision(GameObject pOtherObject)
        {
           
        }
    }
}
