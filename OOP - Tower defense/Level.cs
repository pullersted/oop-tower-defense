﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP___Tower_defense
{
    class Level : GameObject
    {

        public Level(GameWorld pGameWorld, Rectangle pDisplayRectangle, Vector2 pPosition, Image pImageUrl, float pScaleFactor) : base(pGameWorld, pDisplayRectangle, pPosition, pImageUrl, pScaleFactor)
        {
            IsStatic = true;
        }

        public override void OnCollision(GameObject pOtherObject)
        {
            
        }
    }
}
