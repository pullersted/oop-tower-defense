﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace OOP___Tower_defense
{
    internal class poison_slow : Shoot
    {

        private float SlowProcent = 0.3f;
        private float SlowDuration = 1;

        private float PoisonDamage;

        private float PoisonDuration = 2;
        private float PoisonDamageInterval = 0.25f;


        public poison_slow(GameWorld pGameWorld, Rectangle pDisplayRectangle, Vector2 pPosition, Image pImageUrl, float pScaleFactor, Enemy enemy, float damage) : 
            base(pGameWorld, pDisplayRectangle, pPosition, pImageUrl, pScaleFactor, enemy,damage)
        {
            ProjectileSpeed = 400;
            PoisonDamage = damage / (PoisonDuration / PoisonDamageInterval);
        }


        public override void Update(float pDeltaTime)
        {
            base.Update(pDeltaTime);
        }

        public override void Draw(Graphics pGraphics)
        {
            base.Draw(pGraphics);
        }

        protected override void CollidingWithEnemy(Enemy collidingEnemy)
        {
            GameWorld.AddObjectToNextCycle(new Slow(GameWorld, DisplayRectangle, Position, MySprite, 0, TargetEnemy.GetMovement(), SlowProcent, SlowDuration));
            GameWorld.AddObjectToNextCycle(new Poison(GameWorld, DisplayRectangle, Position, MySprite, 0,TargetEnemy,PoisonDamage,PoisonDamageInterval,PoisonDuration));
            Die();
        }
    }
}