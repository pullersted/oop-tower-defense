﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP___Tower_defense
{
    internal class Lightning_attack : Shoot
    {
        public Lightning_attack(GameWorld pGameWorld, Rectangle pDisplayRectangle, Vector2 pPosition, Image pImageUrl, float pScaleFactor, Enemy enemy, float damage) : 
            base(pGameWorld, pDisplayRectangle, pPosition, pImageUrl, pScaleFactor, enemy,damage)
        {
            ProjectileSpeed = 800;

            this.AnimationFrames = GameWorld.Animations["Lightning"];
            AnimationSpeed = 100;

            MySprite = this.AnimationFrames[0];
        }

        public override void Update(float pDeltaTime)
        {
            base.Update(pDeltaTime);
            this.UpdateAnimation(pDeltaTime);
        }
    }
}