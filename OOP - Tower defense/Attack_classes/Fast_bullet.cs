﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP___Tower_defense
{
    internal class Fast_bullet : Shoot
    {
        public Fast_bullet(GameWorld pGameWorld, Rectangle pDisplayRectangle, Vector2 pPosition, Image pImageUrl, float pScaleFactor, Enemy enemy,float damage) : 
            base(pGameWorld, pDisplayRectangle, pPosition, pImageUrl, pScaleFactor, enemy, damage)
        {            
            ProjectileSpeed = 400;
        }
    }
}