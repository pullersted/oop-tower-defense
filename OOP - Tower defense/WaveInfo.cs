﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace OOP___Tower_defense
{
    struct WaveInfo
    {
        public int SpawnNumber;
        public Image Image;
        public float ScaleFactor;
        public float SpawnWaitInterval;
        public Vector2 SpawnPosition;
        public EnemyType EnemyType;

        public WaveInfo(int pSpawnNumber, Image image, float pScaleFactor,float spawnWaitInterval,Vector2 spawnPosition, EnemyType enemyType)
        {
            EnemyType = enemyType;
            SpawnPosition = spawnPosition;
            SpawnWaitInterval = spawnWaitInterval;
            SpawnNumber = pSpawnNumber;
            Image = image;
            ScaleFactor = pScaleFactor;
        }

    }
}
