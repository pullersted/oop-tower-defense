﻿namespace OOP___Tower_defense
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.Timer = new System.Windows.Forms.Timer(this.components);
            this.WaveCounter = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // Timer
            // 
            this.Timer.Enabled = true;
            this.Timer.Interval = 40;
            this.Timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // WaveCounter
            // 
            this.WaveCounter.AutoSize = true;
            this.WaveCounter.BackColor = System.Drawing.Color.Transparent;
            this.WaveCounter.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.WaveCounter.ForeColor = System.Drawing.Color.Red;
            this.WaveCounter.Image = ((System.Drawing.Image)(resources.GetObject("WaveCounter.Image")));
            this.WaveCounter.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.WaveCounter.Location = new System.Drawing.Point(-3, -3);
            this.WaveCounter.Margin = new System.Windows.Forms.Padding(0);
            this.WaveCounter.Name = "WaveCounter";
            this.WaveCounter.Size = new System.Drawing.Size(482, 46);
            this.WaveCounter.TabIndex = 0;
            this.WaveCounter.Text = "                                          ";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(694, 400);
            this.Controls.Add(this.WaveCounter);
            this.Name = "Form1";
            this.Text = "Defense against the demons(DaTD)";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Timer Timer;
        private System.Windows.Forms.Label WaveCounter;
    }
}

