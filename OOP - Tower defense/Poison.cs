﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP___Tower_defense
{
    class Poison : GameObject
    {
        private Enemy TargetEnemy;

        private float PoisonDamage;

        private float PoisonTimer;
        private float PoisonDamageInterval;


        public Poison(GameWorld pGameWorld, Rectangle pDisplayRectangle, Vector2 pPosition, Image pImageUrl, float pScaleFactor, Enemy targetEnemy,float poisonDamage,float poisonDamageInterval,float poisonDuration) :
            base(pGameWorld, pDisplayRectangle, pPosition, pImageUrl, pScaleFactor)
        {
            TargetEnemy = targetEnemy;
            PoisonDamage = poisonDamage;
            PoisonDamageInterval = poisonDamageInterval;
            GameWorld.AddObjectToNextCycle(new DestroyTarget(GameWorld, DisplayRectangle, Position, MySprite, 0, poisonDuration, this));
        }

        public override void Update(float deltaTime)
        {
            PoisonTimer -= deltaTime;
            ShouldPoison();
        }

        void ShouldPoison()
        {
            if (PoisonTimer <= 0)
            {
                PoisonEnemy();
            }
        }

        void PoisonEnemy()
        {
            TargetEnemy.Health -= PoisonDamage;
            PoisonTimer = PoisonDamageInterval;
        }

        public override void OnCollision(GameObject pOtherObject) { }
        public override void Draw(Graphics pGraphics) { }
    }
}
