﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IrrKlang;

namespace OOP___Tower_defense
{
    static class SoundManager
    {
        private static ISoundEngine Engine = new ISoundEngine();

        public static void Init()
        {
            Engine.SoundVolume = 0.5f;
        }

        public static ISoundEngine GetSoundEngine()
        {
            return Engine;
        }

        public static void Play(string pathToSoundFile)
        {
            Engine.Play2D(pathToSoundFile);
        }
        public static void Play(string pathToSoundFile,bool loop)
        {
            Engine.Play2D(pathToSoundFile, loop);
        }

        public static void RemoveSound(string soundFileName)
        {
            Engine.RemoveSoundSource(soundFileName);
        }

        public static void StopAllSounds()
        {
            Engine.StopAllSounds();
        }
    }
}
