﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP___Tower_defense
{
    class EnemySpawner : Enemy
    {
        private List<Vector2> PossibleSpawnDirections = new List<Vector2>()
        {
            new Vector2(1,0),
            new Vector2(-1,0),
            new Vector2(0,1),
            new Vector2(0,-1)
        };

        private int DistanceBetweenEnemies = 25;
        private int EnemiesToSpawn = 2;

        public EnemySpawner(GameWorld pGameWorld, Rectangle pDisplayRectangle, Vector2 pPosition, Image pImageUrl, float pScaleFactor,PathFinder pathFinder) : 
            base(pGameWorld, pDisplayRectangle, pPosition, pImageUrl, pScaleFactor,pathFinder)
        {
            Speed = 40;
            MoneyDrop = 10;
            Health = 10;
            Init();
        }

        void SpawnEnemies()
        {

            for (int tEnemySpawnNumber = 0; tEnemySpawnNumber < EnemiesToSpawn; tEnemySpawnNumber++)
            {
                foreach (Vector2 PossibleSpawnDirection in PossibleSpawnDirections)
                {
                    Vector2 SpawnPosition = Position + ((PossibleSpawnDirection * DistanceBetweenEnemies) * tEnemySpawnNumber);
                    if (CanSpawnEnemy(SpawnPosition))
                    {
                        GameWorld.AddObjectToNextCycle(new EnemyStandard(GameWorld, DisplayRectangle, SpawnPosition, GameWorld.Images["Imp"], 1, PathFinder));
                        break;
                    }
                }
            }
        }

        bool CanSpawnEnemy(Vector2 positionToCheck)
        {
            return !GameWorld.StaticCollision.Exists(item => item.Contains(positionToCheck.ToPointF()));
        }

        public override void Die()
        {
            SpawnEnemies();
            base.Die();
        }
    }
}
