﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace OOP___Tower_defense
{
    abstract class Enemy : GameObject
    {

        protected float MyHealth;
        protected float MaxHealth = 100;
        protected float StartHealth;

        protected float HealthMultiplier = 1.2f;
        protected float MoneyMultiplier = 1.15f;
        protected float Speed;
        protected float MoneyDrop;
        protected PathFinder PathFinder;
        protected Movement MyMovement;
        protected bool UsePathfinding;
        protected bool Flying;
        protected Vector2 Goal;


        public bool IsFlying
        {
            get
            {
                return Flying;
            }
            protected set
            {
                Flying = value;
            }
        }

        public float Health
        {
            get
            {
                return MyHealth;
            }
            set
            {
                float NewHealthValue = value;
                if (NewHealthValue >=  0 && MyHealth <= MaxHealth)
                {
                    MyHealth = value;
                }
                else if(NewHealthValue < 0)
                {
                    MyHealth = 0;
                }
                else if(NewHealthValue > MaxHealth)
                {
                    MyHealth = MaxHealth;
                }
                
            }
        }  

        public Enemy(GameWorld pGameWorld, Rectangle pDisplayRectangle, Vector2 pPosition, Image image, float pScaleFactor,PathFinder pathFinder)
            : base(pGameWorld, pDisplayRectangle, pPosition, image, pScaleFactor)
        {
            PathFinder = pathFinder;
            PathFinder.SetupGrid(new Vector2(CollisionBox.Width, CollisionBox.Height));
            Goal = new Vector2(565, 160);
            UsePathfinding = true;
        }

        public Movement GetMovement()
        {
            return MyMovement;
        }

        
        protected void CalculateStats()
        {
            if(GameManager.Level > 1)
            {
                // Renters rente
                Health *= (float)Math.Pow(HealthMultiplier, GameManager.Level-1);
                MoneyDrop *= (float)Math.Pow(MoneyMultiplier, GameManager.Level-1);
            }
           
        }
        

        protected virtual void Init()
        {
            CalculateStats();
            MyMovement = new Movement(this, PathFinder, Goal, Speed, UsePathfinding);
            StartHealth = Health;

        }

        public override void Reset()
        {
           MyMovement.SetTarget(Goal, UsePathfinding);
            Health = StartHealth;
        }

        public override void OnCollision(GameObject pOtherObject)
        {
            
        }

        public override void Update(float pDeltaTime)
        {
            base.Update(pDeltaTime);
            CheckDeathConditions();
            MyMovement.Update(pDeltaTime);                    
        }

        protected virtual void CheckDeathConditions()
        {
            if ((UsePathfinding && !MyMovement.IsMoving) || Goal.X <= Position.X)
            {
                GameManager.Life--;
                Die();
            }
            if(MyHealth <= 0)
            {
                GameManager.Money += MoneyDrop;
                Die();
            }
        }
    }
}
