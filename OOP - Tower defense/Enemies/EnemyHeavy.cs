﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP___Tower_defense
{
    class EnemyHeavy : Enemy
    {
        public EnemyHeavy(GameWorld pGameWorld, Rectangle pDisplayRectangle, Vector2 pPosition, Image pImageUrl, float pScaleFactor,PathFinder pathFinder) : 
            base(pGameWorld, pDisplayRectangle, pPosition, pImageUrl, pScaleFactor, pathFinder)
        {
            Speed = 30;
            Health = 15;
            MoneyDrop = 20;
            Init();
        }
    }
}
