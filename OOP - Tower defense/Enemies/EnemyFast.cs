﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP___Tower_defense
{
    class EnemyFast : Enemy
    {
        

        public EnemyFast(GameWorld pGameWorld, Rectangle pDisplayRectangle, Vector2 pPosition, Image pImageUrl, float pScaleFactor,PathFinder pathFinder) : 
            base(pGameWorld, pDisplayRectangle, pPosition, pImageUrl, pScaleFactor, pathFinder)
        {
            Speed = 100;
            Health = 2;
            MoneyDrop = 4f;
            Init();        
        }

    }
}
