﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP___Tower_defense
{
    class EnemyStandard : Enemy
    {
        public EnemyStandard(GameWorld pGameWorld, Rectangle pDisplayRectangle, Vector2 pPosition, Image pImageUrl, float pScaleFactor,PathFinder pathFinder) : 
            base(pGameWorld, pDisplayRectangle, pPosition, pImageUrl, pScaleFactor, pathFinder)
        {
            Speed = 50;
            Health = 3;
            MoneyDrop = 8;
            Init();
        }
    }
}