﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP___Tower_defense
{
    class WaveManager
    {
        private WaveSpawner mWaveSpawner;
        private GameWorld GameWorld;

        public WaveManager(WaveSpawner pWaveSpawner,GameWorld gameWorld)
        {
            mWaveSpawner = pWaveSpawner;
            GameWorld = gameWorld;
        }

        public void SpawnWave(int pWaveNumber,Vector2 pPositionToSpawnAt)
        {
            List<WaveInfo> mWaveSpawnerList = new List<WaveInfo>();

            switch(pWaveNumber)
            {
                case 1:
                    mWaveSpawnerList.Add(new WaveInfo(5, GameWorld.Images["Imp"], 1f,0.75f, pPositionToSpawnAt, EnemyType.Standard));                    
                    break;
                case 2:
                    mWaveSpawnerList.Add(new WaveInfo(5, GameWorld.Images["Imp"], 1f,0.75f, pPositionToSpawnAt, EnemyType.Standard));
                    mWaveSpawnerList.Add(new WaveInfo(3, GameWorld.Images["Speedie"], 1, 1, pPositionToSpawnAt, EnemyType.Fast));
                    break;
                case 3:
                    mWaveSpawnerList.Add(new WaveInfo(5, GameWorld.Images["Imp"], 1f, 0.75f, pPositionToSpawnAt, EnemyType.Standard));
                    mWaveSpawnerList.Add(new WaveInfo(6, GameWorld.Images["Speedie"],1, 0.5f, pPositionToSpawnAt, EnemyType.Fast));
                   
                    break;
                case 4:
                    mWaveSpawnerList.Add(new WaveInfo(10, GameWorld.Images["Imp"], 1f, 0.5f, pPositionToSpawnAt, EnemyType.Standard));
                    mWaveSpawnerList.Add(new WaveInfo(10, GameWorld.Images["Speedie"], 1, 0.3f, pPositionToSpawnAt, EnemyType.Fast));
                    break;
                case 5:
                    mWaveSpawnerList.Add(new WaveInfo(15, GameWorld.Images["Speedie"], 1f, 0.35f, pPositionToSpawnAt, EnemyType.Fast));
                    mWaveSpawnerList.Add(new WaveInfo(1, GameWorld.Images["Saucer"], 1f, 0.5f, pPositionToSpawnAt, EnemyType.Flying));
                    break;
                case 6:
                    mWaveSpawnerList.Add(new WaveInfo(10, GameWorld.Images["Imp"], 1f, 0.75f, pPositionToSpawnAt, EnemyType.Standard));
                    mWaveSpawnerList.Add(new WaveInfo(5, GameWorld.Images["Berry"], 0.75f, 0.75f, pPositionToSpawnAt, EnemyType.Spawner));
                    break;
                case 7:
                    mWaveSpawnerList.Add(new WaveInfo(20, GameWorld.Images["Speedie"], 1f, 0.35f, pPositionToSpawnAt, EnemyType.Fast));
                    mWaveSpawnerList.Add(new WaveInfo(3, GameWorld.Images["HeavyEnemy"], 0.75f, 1, pPositionToSpawnAt, EnemyType.Heavy));
                    break;
                case 8:
                    mWaveSpawnerList.Add(new WaveInfo(3, GameWorld.Images["HeavyEnemy"], 0.75f, 1, pPositionToSpawnAt, EnemyType.Heavy));
                    mWaveSpawnerList.Add(new WaveInfo(8, GameWorld.Images["Saucer"], 1f, 0.75f, pPositionToSpawnAt, EnemyType.Flying));
                    break;
                case 9:
                    mWaveSpawnerList.Add(new WaveInfo(50, GameWorld.Images["Speedie"], 1f, 0.2f, pPositionToSpawnAt, EnemyType.Fast));
                    break;
                case 10:
                    mWaveSpawnerList.Add(new WaveInfo(10, GameWorld.Images["HeavyEnemy"], 0.75f, 1, pPositionToSpawnAt, EnemyType.Heavy));
                    mWaveSpawnerList.Add(new WaveInfo(5, GameWorld.Images["Saucer"], 1f, 0.5f, pPositionToSpawnAt, EnemyType.Flying));
                    mWaveSpawnerList.Add(new WaveInfo(20, GameWorld.Images["Berry"], 0.75f, 0.75f, pPositionToSpawnAt, EnemyType.Spawner));
                    mWaveSpawnerList.Add(new WaveInfo(5, GameWorld.Images["Saucer"], 1f, 0.35f, pPositionToSpawnAt, EnemyType.Flying));
                    GameManager.LastWave = true;
                    break;
                
            }


            mWaveSpawner.Spawn(mWaveSpawnerList);
        }

    }
}
