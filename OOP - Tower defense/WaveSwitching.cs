﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace OOP___Tower_defense
{
    class WaveSwitching
    {

        private int TimeBetweenWaves = 5;
        private int FirstTimeWave = 3;
        private GameWorld GameWorld;
        private WaveSpawner WaveSpawner;

        private bool IsSwitchingWave;
        private float WaveTimer;
        private Label WaveCounter;

        public WaveSwitching(GameWorld gameWorld,WaveSpawner waveSpawner)
        {
            WaveTimer = FirstTimeWave;
            IsSwitchingWave = true;
            GameWorld = gameWorld;
            WaveSpawner = waveSpawner;
            WaveCounter = (Label)Form1.Self.Controls["WaveCounter"];
        }

        public void Update(float deltaTime)
        {
            if(ShouldSwitch() && WaveTimer <= 0 && !IsSwitchingWave && WaveSpawner.IsDoneSpawning)
            {
                WaveTimer = TimeBetweenWaves;
                IsSwitchingWave = true;
            }
            if(WaveTimer > 0)
            {
                WaveCounter.Text = "          Next wave in : " + WaveTimer.ToString("F1") + "\n" + 
                                   "          Wave number " + (GameManager.Level + 1).ToString("F0");
                WaveTimer -= deltaTime;
            }
            if(IsSwitchingWave && WaveTimer <= 0 && WaveSpawner.IsDoneSpawning)
            {
                GameManager.Level++;
                GameWorld.NewWave();
                IsSwitchingWave = false;
                WaveCounter.Text = "";


            }


        }

        bool ShouldSwitch()
        {
            return !GameWorld.Objects.Exists(item => item is Enemy);
        }
    }
}
