﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;


namespace OOP___Tower_defense
{
    public class GameWorld
    {
        public List<GameObject> Objects = new List<GameObject>();
        public Dictionary<string, Image> Images = new Dictionary<string, Image>();
        public Dictionary<string, List<Image>> Animations = new Dictionary<string, List<Image>>();
        public List<RectangleF> StaticCollision = new List<RectangleF>();

        private DateTime EndTime;
        private int CurrentFPS;
        private float DeltaTime;
        public Graphics Graphics;
        private BufferedGraphics BackBuffer;
        private Rectangle DisplayRectangle;
        private PathFinder PathFinder;
        private WaveManager WaveManager;
        private WaveSpawner WaveSpawner;
        private WaveSwitching WaveSwitching;

        private List<GameObject> ObjectsToAddInNextCycle = new List<GameObject>();
        private List<GameObject> ObjectsToRemoveInNextCycle = new List<GameObject>();


        public GameWorld(Graphics graphics, Rectangle displayRectangle)
        {
            DisplayRectangle = displayRectangle;
            BackBuffer = BufferedGraphicsManager.Current.Allocate(graphics, displayRectangle);
            Graphics = BackBuffer.Graphics;
            AddImages();
            AddAnimations();
            SetupWorld();
        }

        void AddAnimations()
        {
            Animations.Add("Explosion",GetAnimationFromPath(@"Explosion\EP (1).png;Explosion\EP (2).png;Explosion\EP (3).png;Explosion\EP (4).png;"+
                        @"Explosion\EP (5).png;Explosion\EP (6).png;Explosion\EP (7).png;Explosion\EP (8).png;Explosion\EP (9).png"));

            Animations.Add("Lightning", GetAnimationFromPath(@"Projectiles\ligtning1.png;Projectiles\ligtning2.png;Projectiles\ligtning3.png;" +
                       @"Projectiles\ligtning4.png;Projectiles\ligtning5.png;Projectiles\ligtning6.png"));
        }

        List<Image> GetAnimationFromPath(string animationPath)
        {
            List<Image> Animation = new List<Image>();
            foreach (string AnimationPath in animationPath.Split(';'))
            {
                Animation.Add(Image.FromFile(AnimationPath));
            }
            return Animation;
        }

        void AddImages()
        {
            Debug.WriteLine(Environment.CurrentDirectory);
            Images.Add("Green", Image.FromFile(@"Images\green.png"));
            Images.Add("GraySquare", Image.FromFile(@"Images\sqaure.png"));
            Images.Add("Tower", Image.FromFile(@"Images\Tower.png"));
            Images.Add("Gun", Image.FromFile(@"Images\gun_a.png"));
            Images.Add("Enemy", Image.FromFile(@"Images\flying_enemy.jpg"));
            Images.Add("Background", Image.FromFile(@"Images\BFjwi.png"));
            Images.Add("Heavy", Image.FromFile(@"Images\rocket.png"));
            Images.Add("Rocket", Image.FromFile(@"Projectiles\Rocket.png"));
            Images.Add("Glue", Image.FromFile(@"Images\glue.png"));
            Images.Add("TwoGun", Image.FromFile(@"Images\gun.png"));
            Images.Add("Imp", Image.FromFile(@"Images\imp.png"));
            Images.Add("Berry", Image.FromFile(@"Images\berry_s2.png"));
            Images.Add("HeavyEnemy", Image.FromFile(@"Images\heavy_h10.png"));
            Images.Add("Saucer", Image.FromFile(@"Images\saucer.png"));
            Images.Add("Speedie", Image.FromFile(@"Images\speedie.png"));
            Images.Add("Tesla", Image.FromFile(@"Images\Tesla.png"));
            Images.Add("UpgradeButtonImage", Image.FromFile(@"Shop and wave sprites\Upgrade.png"));
            Images.Add("SellButtonImage", Image.FromFile(@"Shop and wave sprites\Sell.png"));
            Images.Add("Bullet_Sort", Image.FromFile(@"Projectiles\Bullet_Sort.png"));
            Images.Add("Town", Image.FromFile(@"Shop and wave sprites\Town.png"));
            


        }

        public virtual void UpdateAnimations(float currentFPS)
        {
            foreach (GameObject go in Objects)
            {
                if(go.HasAnimation)
                {
                    go.UpdateAnimation(currentFPS);
                }
               
            }
        }

        

        public void SetupWorld()
        {
            Objects.Add(new Level(this, DisplayRectangle, new Vector2(0, 0), Images["Background"], 1f));
            Objects.Add(new StaticObject(this, DisplayRectangle, new Vector2(550, 145), Images["Town"], 0.6f));

            StaticCollision.Add(new RectangleF(-1, 0, 80, 200)); //x, y, width, height
            StaticCollision.Add(new RectangleF(80, 0, 159, 81));
            StaticCollision.Add(new RectangleF(239, 0, 356, 161));
            StaticCollision.Add(new RectangleF(239, 140, 120, 100));
            //under stien v
            StaticCollision.Add(new RectangleF(0, 236, 120, 164));
            StaticCollision.Add(new RectangleF(120, 120, 80, 281));
            StaticCollision.Add(new RectangleF(200, 278, 200, 122));
            StaticCollision.Add(new RectangleF(398, 200, 199, 200));

            DropTowerInfo DropTowerInfo = new DropTowerInfo(50, TowerType.Standard, Images["Gun"], 1f);
            Objects.Add(
                new BuyTowerButton(this, DisplayRectangle, new Vector2(610, 50),Images["Gun"], 1f, DropTowerInfo.Cost.ToString(), DropTowerInfo,"Standard"));

            DropTowerInfo = new DropTowerInfo(50, TowerType.Fast, Images["TwoGun"], 1f);
            Objects.Add(new BuyTowerButton(this, DisplayRectangle, new Vector2(655, 50), Images["TwoGun"], 1f, DropTowerInfo.Cost.ToString(), DropTowerInfo, "High attack speed, low damage. \n Can hit flying enemies"));

            DropTowerInfo = new DropTowerInfo(100, TowerType.Heavy, Images["Heavy"], 1f);
            Objects.Add(new BuyTowerButton(this, DisplayRectangle, new Vector2(610, 100), Images["Heavy"],1f, DropTowerInfo.Cost.ToString(),DropTowerInfo, "Low attack speed, high damage. \n Can hit flying enemies"));

            DropTowerInfo = new DropTowerInfo(75, TowerType.Poison, Images["Glue"], 1f);
            Objects.Add(new BuyTowerButton(this, DisplayRectangle, new Vector2(655, 100), Images["Glue"], 1, DropTowerInfo.Cost.ToString(), DropTowerInfo, "Slows and poison the enemy"));


            DropTowerInfo = new DropTowerInfo(125, TowerType.AOE, Images["Tesla"], 1f);
            Objects.Add(new BuyTowerButton(this, DisplayRectangle, new Vector2(610, 150), Images["Tesla"], 1f, DropTowerInfo.Cost.ToString(),DropTowerInfo, "Hits multiple targets"));

            PathFinder = new PathFinder(this, new Vector2(64, 64), 16);
            WaveSpawner = new WaveSpawner(this, DisplayRectangle, PathFinder);
            WaveManager = new WaveManager(WaveSpawner,this);
            WaveSwitching = new WaveSwitching(this,WaveSpawner);         
        }

        public void GameLoop()
        {
            DateTime tStartTime = DateTime.Now;

            TimeSpan tDeltaTime = tStartTime - EndTime;

            CurrentFPS = tDeltaTime.Milliseconds > 0 ? tDeltaTime.Milliseconds : 1;
            DeltaTime = (float)tDeltaTime.Milliseconds / (float)1000;

            AddObjects();
            RemoveObjects();

            Draw(Graphics);

            Update(DeltaTime);
            UpdateAnimations(DeltaTime);

            EndTime = DateTime.Now;
        }

        public void NewWave()
        {
            WaveManager.SpawnWave(GameManager.Level, new Vector2(0, 210));
        }

        public void AddObjectToNextCycle(GameObject objectToAdd)
        {
            ObjectsToAddInNextCycle.Add(objectToAdd);
        }

        public void RemoveObjectInNextCycle(GameObject objectToRemove)
        {
            ObjectsToRemoveInNextCycle.Add(objectToRemove);
        }

        private void AddObjects()
        {
            foreach (GameObject tObjectToAdd in ObjectsToAddInNextCycle)
            {
                if(!Objects.Contains(tObjectToAdd))
                {
                    Objects.Add(tObjectToAdd);
                }
               
            }
            ObjectsToAddInNextCycle.Clear();
        }

        private void RemoveObjects()
        {
            foreach (GameObject ObjectToRemove in ObjectsToRemoveInNextCycle)
            {
                if (Objects.Contains(ObjectToRemove))
                {
                    Objects.Remove(ObjectToRemove);         
                }
            }
            ObjectsToRemoveInNextCycle.Clear();
        }

        private void Draw(Graphics graphics)
        {
            Graphics.Clear(Color.White);

            foreach (GameObject tObject in Objects)
            {
                tObject.Draw(graphics);
            }
            Font f = new Font("Arial", 12);


#if DEBUG
            Graphics.DrawString(" FPS" + CurrentFPS, f, Brushes.Black, 0, 0);
#endif

            Graphics.DrawString("Money: " + GameManager.Money.ToString("F0"), f, Brushes.Black, 598, 25);
            Graphics.DrawString("Life: " + GameManager.Life, f, Brushes.Black, 598, 0);
            BackBuffer.Render();
        }

        void ShouldWin()
        {
            if(GameManager.LastWave && WaveSpawner.IsDoneSpawning && !Objects.Exists(item => item is Enemy))
            {
                GameManager.Win();
            }
        }

        void ShouldLose()
        {
            if(GameManager.Life <= 0)
            {
                GameManager.Lose();
            }
        }

        private void Update(float deltaTime)
        {
            foreach (GameObject tObject in Objects)
            {
                tObject.Update(deltaTime);
            }
            WaveSpawner.Update(deltaTime);
            WaveSwitching.Update(deltaTime);
            ShouldLose();
            ShouldWin();
        }
    }
}
