﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;
using System.Diagnostics;

namespace OOP___Tower_defense
{
    public enum TowerType
    {
        Standard,
        AOE,
        Fast,
        Heavy,
        Poison
    }

    class DragNDropObject : GameObject
    {
        private bool mCanDrop = true;
        private TowerType mTowerType;
        private Image mImageUrl;
        private int mCost;
        private Dictionary<TowerType, Tower> Towers = new Dictionary<TowerType, Tower>();

        public DragNDropObject(GameWorld pGameWorld, Rectangle pDisplayRectangle, Vector2 pPosition, Image pImageUrl, float pScaleFactor,TowerType pTowerType,int pCost) :
            base(pGameWorld,pDisplayRectangle,pPosition,pImageUrl,pScaleFactor)
        {
            mCost = pCost;
            mImageUrl = pImageUrl;
            mTowerType = pTowerType;
            InitTowerList();
        }

        void InitTowerList()
        {
            Towers.Add(TowerType.Standard, new TowerStandard(GameWorld, DisplayRectangle, Position, MySprite, ScaleFactor,GameWorld.Images["Bullet_Sort"]));
            Towers.Add(TowerType.Poison, new TowerPoison(GameWorld, DisplayRectangle, Position, MySprite, ScaleFactor, GameWorld.Images["Imp"]));
            Towers.Add(TowerType.Heavy, new TowerHeavy(GameWorld, DisplayRectangle, Position, MySprite, ScaleFactor, GameWorld.Images["Rocket"]));
            Towers.Add(TowerType.Fast, new TowerFast(GameWorld, DisplayRectangle, Position, MySprite, ScaleFactor, GameWorld.Images["Bullet_Sort"]));
            Towers.Add(TowerType.AOE, new TowerAoe(GameWorld, DisplayRectangle, Position, MySprite, ScaleFactor,GameWorld.Images["Imp"]));
        }

        public override void Update(float pDeltaTime)
        {
            
            Position = new Vector2(Form1.TheMousePosition.X-(MySprite.Width * ScaleFactor)/2, Form1.TheMousePosition.Y - (MySprite.Height * ScaleFactor) / 2);
     
            if(Form1.MouseButtons == MouseButtons.None && mCanDrop)
            {
                Drop();                
            }
            
        }

        void Drop()
        {
            if(CollidingOnAllFourCorners())
            {
                Debug.WriteLine("SUCCESS DROP");
                GameManager.Money -= mCost;
                Tower TowerToSpawn = Towers[mTowerType];
                TowerToSpawn.Position = Position;
                TowerToSpawn.Init(mCost);
                GameWorld.AddObjectToNextCycle(TowerToSpawn);
            }
            else if(!CollidingOnAllFourCorners())
            {
                Debug.WriteLine("FAILED DROP");
            }
            GameWorld.RemoveObjectInNextCycle(this);
        }

        bool CollidingOnAllFourCorners()
        {
            Vector2[] tPointsToCheck = new Vector2[4];
            // Up left corner
            tPointsToCheck[0] = new Vector2(Position.X, Position.Y);
            // Down right corner
            tPointsToCheck[1] = new Vector2(Position.X+MySprite.Width * ScaleFactor, Position.Y + MySprite.Height * ScaleFactor);
            // Up right corner
            tPointsToCheck[2] = new Vector2(Position.X + MySprite.Width * ScaleFactor, Position.Y);
            // Down left corner
            tPointsToCheck[3] = new Vector2(Position.X, Position.Y + MySprite.Height * ScaleFactor);
            bool tCollidingWithSomething = true;
            for(int tPointNumber = 0;tPointNumber < tPointsToCheck.Length;tPointNumber++)
            {
                if (GameWorld.StaticCollision.Exists(item => item.Contains(tPointsToCheck[tPointNumber].ToPointF())) && tCollidingWithSomething &&
                    GameWorld.Objects.Find(item => item != this && !item.IsStatic && item is Tower && item.CollisionBox.Contains(tPointsToCheck[tPointNumber].ToPointF())) == null)
                {
                    tCollidingWithSomething = true;
                    continue;
                }
                else
                {
                    tCollidingWithSomething = false;
                    break;
                }
            }


            return tCollidingWithSomething;
        }

        public override void OnCollision(GameObject pOtherObject)
        {
            
        }
    }
}
