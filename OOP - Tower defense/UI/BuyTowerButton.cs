﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Diagnostics;
using System.Windows.Forms;

namespace OOP___Tower_defense
{
    class BuyTowerButton : Button
    {
        private DropTowerInfo mDropTowerInfo;
        private int XTextOffset;

        private ToolTip Tooltip;
        private string Description;
        

        public BuyTowerButton(GameWorld pGameWorld, Rectangle pDisplayRectangle, Vector2 pPosition, Image pImageUrl, float pScaleFactor, string pButtonName, DropTowerInfo pDropTowerInfo,string description) :
            base(pGameWorld,pDisplayRectangle,pPosition,pImageUrl,pScaleFactor,pButtonName)
        {
            mDropTowerInfo = pDropTowerInfo;
            XTextOffset = -10;
            Description = description;
            Tooltip = new ToolTip();
        }

        protected override void OnMouseStay()
        {
            base.OnMouseStay();
            if(!MouseStay)
            {
                Tooltip.Show(Description, Form1.Self);
            }
            
        }

        public override void Update(float pDeltaTime)
        {
            base.Update(pDeltaTime);
            if (Form1.ClickedOnScreen)
            {
                Tooltip.Hide(Form1.Self);
            }
        }

        protected override void OnMouseLeave()
        {
            base.OnMouseLeave();
            Tooltip.Hide(Form1.Self);
        }

        protected override void OnMouseDown()
        {
            if(GameManager.Money >= mDropTowerInfo.Cost)
            {      
                GameWorld.AddObjectToNextCycle(new DragNDropObject(GameWorld, DisplayRectangle, Position, mDropTowerInfo.ImageUrl, mDropTowerInfo.ScaleFactor, mDropTowerInfo.TowerType,mDropTowerInfo.Cost));
            }
            
        }

        public override void Draw(Graphics pGraphics)
        {
            base.Draw(pGraphics);
            pGraphics.DrawString(ButtonText, FontToUse, new SolidBrush(Color.Black),
                new Vector2(Position.X + (MySprite.Width * ScaleFactor)/2 + XTextOffset, Position.Y + (MySprite.Width * ScaleFactor / 2) + FontToUse.Height).ToPointF());

        }

    }
}
