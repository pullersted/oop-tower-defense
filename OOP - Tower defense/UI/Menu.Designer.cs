﻿namespace OOP___Tower_defense
{
    partial class Menu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Menu));
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.developersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nicholaiWestToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.emilNiemannToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.phillipUllerstedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contactUsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.philipullerstedgmailcomToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.zorgus2gmailcomToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nicholaiwphotmailcomToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.Lime;
            this.button2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button2.Location = new System.Drawing.Point(240, 223);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(122, 55);
            this.button2.TabIndex = 1;
            this.button2.Text = "How To Play";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            this.button2.MouseEnter += new System.EventHandler(this.button2_MouseEnter);
            this.button2.MouseLeave += new System.EventHandler(this.button2_MouseLeave);
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.Red;
            this.button3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button3.Location = new System.Drawing.Point(475, 223);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(122, 57);
            this.button3.TabIndex = 2;
            this.button3.Text = "Quit Game";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            this.button3.MouseEnter += new System.EventHandler(this.button3_MouseEnter);
            this.button3.MouseLeave += new System.EventHandler(this.button3_MouseLeave);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Lime;
            this.button1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button1.Location = new System.Drawing.Point(0, 262);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(97, 59);
            this.button1.TabIndex = 4;
            this.button1.Text = "Start Game";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            this.button1.MouseEnter += new System.EventHandler(this.button1_MouseEnter);
            this.button1.MouseLeave += new System.EventHandler(this.button1_MouseLeave);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Cursor = System.Windows.Forms.Cursors.Default;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(0, 27);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(597, 382);
            this.pictureBox1.TabIndex = 3;
            this.pictureBox1.TabStop = false;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.developersToolStripMenuItem,
            this.contactUsToolStripMenuItem,
            this.toolStripMenuItem1});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(597, 24);
            this.menuStrip1.TabIndex = 5;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // developersToolStripMenuItem
            // 
            this.developersToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nicholaiWestToolStripMenuItem,
            this.emilNiemannToolStripMenuItem,
            this.phillipUllerstedToolStripMenuItem});
            this.developersToolStripMenuItem.Name = "developersToolStripMenuItem";
            this.developersToolStripMenuItem.Size = new System.Drawing.Size(77, 20);
            this.developersToolStripMenuItem.Text = "Developers";
            // 
            // nicholaiWestToolStripMenuItem
            // 
            this.nicholaiWestToolStripMenuItem.Name = "nicholaiWestToolStripMenuItem";
            this.nicholaiWestToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.nicholaiWestToolStripMenuItem.Text = "Nicholai West ";
            // 
            // emilNiemannToolStripMenuItem
            // 
            this.emilNiemannToolStripMenuItem.Name = "emilNiemannToolStripMenuItem";
            this.emilNiemannToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.emilNiemannToolStripMenuItem.Text = "Emil Niemann";
            // 
            // phillipUllerstedToolStripMenuItem
            // 
            this.phillipUllerstedToolStripMenuItem.Name = "phillipUllerstedToolStripMenuItem";
            this.phillipUllerstedToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.phillipUllerstedToolStripMenuItem.Text = "Phillip Ullersted";
            // 
            // contactUsToolStripMenuItem
            // 
            this.contactUsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.philipullerstedgmailcomToolStripMenuItem,
            this.zorgus2gmailcomToolStripMenuItem,
            this.nicholaiwphotmailcomToolStripMenuItem});
            this.contactUsToolStripMenuItem.Name = "contactUsToolStripMenuItem";
            this.contactUsToolStripMenuItem.Size = new System.Drawing.Size(77, 20);
            this.contactUsToolStripMenuItem.Text = "Contact Us";
            // 
            // philipullerstedgmailcomToolStripMenuItem
            // 
            this.philipullerstedgmailcomToolStripMenuItem.Name = "philipullerstedgmailcomToolStripMenuItem";
            this.philipullerstedgmailcomToolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.philipullerstedgmailcomToolStripMenuItem.Text = "philipullersted@gmail.com";
            // 
            // zorgus2gmailcomToolStripMenuItem
            // 
            this.zorgus2gmailcomToolStripMenuItem.Name = "zorgus2gmailcomToolStripMenuItem";
            this.zorgus2gmailcomToolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.zorgus2gmailcomToolStripMenuItem.Text = "zorgus2@gmail.com";
            // 
            // nicholaiwphotmailcomToolStripMenuItem
            // 
            this.nicholaiwphotmailcomToolStripMenuItem.Name = "nicholaiwphotmailcomToolStripMenuItem";
            this.nicholaiwphotmailcomToolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.nicholaiwphotmailcomToolStripMenuItem.Text = "Nicholai_wp@hotmail.com";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(12, 20);
            // 
            // Menu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(597, 382);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.pictureBox1);
            this.Cursor = System.Windows.Forms.Cursors.Hand;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Menu";
            this.Text = "Menu ";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem developersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nicholaiWestToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem emilNiemannToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem phillipUllerstedToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem contactUsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem philipullerstedgmailcomToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem zorgus2gmailcomToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nicholaiwphotmailcomToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
    }
}

