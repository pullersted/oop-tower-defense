﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;


namespace OOP___Tower_defense
{
    struct DropTowerInfo
    {
        public int Cost;
        public TowerType TowerType;
        public Image ImageUrl;
        public float ScaleFactor;

        public DropTowerInfo(int pCost,TowerType pTowerType,Image pImageUrl,float pScaleFactor)
        {
            Cost = pCost;
            TowerType = pTowerType;
            ImageUrl = pImageUrl;
            ScaleFactor = pScaleFactor;
        }


    }
}
