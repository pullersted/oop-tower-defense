﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace OOP___Tower_defense
{
    class UpgradeTowerButton : Button
    {

        Tower TowerToUpgrade;
        Queue<TowerStat> TowerStats;
        private int TextXOffset = -8;
        private int TextYOffset = -37;
        private ToolTip UpgradeToolTip;

        public UpgradeTowerButton(GameWorld pGameWorld, Rectangle pDisplayRectangle, Vector2 pPosition, Image pImageUrl, float pScaleFactor, string pButtonName,TowerInfo towerUpgradeInfo) :
            base(pGameWorld,pDisplayRectangle,pPosition,pImageUrl,pScaleFactor,pButtonName)
        {
            TowerToUpgrade = towerUpgradeInfo.Tower;
            TowerStats = towerUpgradeInfo.TowerUpgradeStats;
            UpgradeToolTip = new ToolTip();
        }

        protected override void OnMouseStay()
        {
            if(TowerStats.Count > 0)
            {
                UpgradeToolTip.Show(TowerStats.Peek().Description, Form1.Self);
            }           
        }

        protected override void OnMouseDown()
        {
            if (TowerStats.Count > 0)
            {
                int Cost = TowerStats.Peek().Cost;

                if(Cost <= GameManager.Money)
                {
                    GameManager.Money -= Cost;
                    TowerToUpgrade.TowerStat += TowerStats.Dequeue();
                    Die();
                }
            }
            
           
        }
        public override void Update(float pDeltaTime)
        {
            base.Update(pDeltaTime);
            if(Form1.ClickedOnScreen)
            {
                UpgradeToolTip.Hide(Form1.Self);
            }
        }

        public override void Draw(Graphics pGraphics)
        {
            if (TowerStats.Count > 0)
            {
                base.Draw(pGraphics);           
                pGraphics.DrawString(TowerStats.Peek().Cost.ToString(), FontToUse, Brushes.Black, (Position + new Vector2(MySprite.Width * ScaleFactor / 2 + TextXOffset, MySprite.Height * ScaleFactor + TextYOffset)).ToPointF());
            }           
        }

        protected override void OnMouseLeave()
        {
            UpgradeToolTip.Hide(Form1.Self);
            Die();
        }
    }
}
