﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;
using System.Diagnostics;

namespace OOP___Tower_defense
{
    abstract class Button : GameObject
    {
        public bool MouseStay;
        public bool MouseDown;

        protected string ButtonText;
        protected int FontSize = 8;
        protected int XOffset = 15;
        protected Font FontToUse;

        public Button(GameWorld pGameWorld, Rectangle pDisplayRectangle, Vector2 pPosition, Image pImageUrl, float pScaleFactor, string buttonText) :
            base(pGameWorld,pDisplayRectangle,pPosition,pImageUrl,pScaleFactor)
        {
            this.ButtonText = buttonText;
            FontToUse  = new Font("Arial", FontSize);
        }

        public override void OnCollision(GameObject pOtherObject)
        {
            // Do nothing
        }

        public override void Update(float pDeltaTime)
        {
            base.Update(pDeltaTime);
            CheckMouseStay();
            CheckMouseDown();
            CheckMouseUp();

        }

        void CheckMouseDown()
        {
            List<Button> tButtons = GameWorld.Objects.FindAll(item => item is Button).Cast<Button>().ToList();
            if (MouseStay && Form1.MouseButtons == MouseButtons.Left && !tButtons.Exists(item => item.MouseDown))
            {
                OnMouseDown();
                MouseDown = true;
            }
        }

        void CheckMouseUp()
        {
            if (MouseDown && Form1.MouseButtons == MouseButtons.None)
            {
                OnMouseUp();
                MouseDown = false;
            }
        }


        void CheckMouseStay()
        {
            List<Button> tButtons = GameWorld.Objects.FindAll(item => item is Button).Cast<Button>().ToList();
            if (CollisionBox.Contains(Form1.TheMousePosition.ToPointF()) && !tButtons.Exists(item => item.MouseStay && item != this))
            {
                OnMouseStay();
                MouseStay = true;
            }
            else if(MouseStay)
            {
                OnMouseLeave();
                MouseStay = false;
            }
        }

        public override void Reset()
        {
            base.Reset();
            MouseStay = false;
            MouseDown = false;
        }

        protected virtual void OnMouseDown()
        {
           
        }

        protected virtual void OnMouseStay()
        {
           
        }

        protected virtual void OnMouseLeave()
        {

        }

        protected virtual void OnMouseUp()
        {

        }

    }
}
