﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using IrrKlang;

namespace OOP___Tower_defense
{
    public partial class Menu : Form
    {
        public Menu()
        {
            SoundManager.Init();
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //How to play
            How_to_play ht = new How_to_play();
            ht.Show();
            this.Hide();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            //Quit Game
            this.Close();
        }

   

        private void button1_MouseEnter(object sender, EventArgs e)
        {
            // This is the hover color of the button
            button1.BackColor = Color.Blue;
        }

        private void button1_MouseLeave(object sender, EventArgs e)
        {
            // This is the hover color of the button
            button1.BackColor = Color.Lime;
        }

        private void button2_MouseEnter(object sender, EventArgs e)
        {
            // This is the hover color of the button
            button2.BackColor = Color.Blue;
        }

        private void button2_MouseLeave(object sender, EventArgs e)
        {
            // This is the hover color of the button
            button2.BackColor = Color.Lime;
        }

        private void button3_MouseEnter(object sender, EventArgs e)
        {
            // This is the hover color of the button
            button3.BackColor = Color.Blue;
        }

        private void button3_MouseLeave(object sender, EventArgs e)
        {
            // This is the hover color of the button
            button3.BackColor = Color.Red;
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            this.Hide();
            Form1 GameForm = new Form1();
            GameForm.Location = this.Location;
            GameForm.Show();
            //Play Game
            //placer metode her og forbind til spillet.
        }
    }
}