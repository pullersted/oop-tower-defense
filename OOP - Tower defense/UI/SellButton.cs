﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace OOP___Tower_defense
{
    class SellButton : Button
    {
        private Tower TowerToSell;
        private int MoneyToGet;
        private int TextXOffset = -7;

        public SellButton(GameWorld pGameWorld, Rectangle pDisplayRectangle, Vector2 pPosition, Image pImageUrl, float pScaleFactor, string pButtonName,Tower towerToSell,int moneyToGet) :
            base(pGameWorld,pDisplayRectangle,pPosition,pImageUrl,pScaleFactor,pButtonName)
        {
            TowerToSell = towerToSell;
            MoneyToGet = moneyToGet;
        }

        protected override void OnMouseDown()
        {
            GameManager.Money += MoneyToGet;
            TowerToSell.Die();
            Die();
        }

        protected override void OnMouseLeave()
        {
            Die();
        }

        public override void Draw(Graphics pGraphics)
        {
            base.Draw(pGraphics);
            pGraphics.DrawString(TowerToSell.SellMoney.ToString(), FontToUse, Brushes.Black, (Position + new Vector2(MySprite.Width * ScaleFactor / 2 + TextXOffset, 0)).ToPointF());

        }

    }
}
