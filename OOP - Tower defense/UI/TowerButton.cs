﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace OOP___Tower_defense
{
    struct TowerButtonInfo
    {
        public Image Image;
        public Button Button;

        public TowerButtonInfo(Image image,Button button)
        {
            Image = image;
            Button = button;
        }
    }

    class TowerButton : Button
    {
        private TowerInfo TowerUpgradeInfo;

        private List<TowerButtonInfo> ButtonsInfo = new List<TowerButtonInfo>();

        public TowerButton(GameWorld pGameWorld, Rectangle pDisplayRectangle, Vector2 pPosition, Image pImageUrl, float pScaleFactor, string pButtonName, TowerInfo towerUpgradeInfo) :
            base(pGameWorld,pDisplayRectangle,pPosition,pImageUrl,pScaleFactor,pButtonName)
        {
            TowerUpgradeInfo = towerUpgradeInfo;
            AddButtons();
        }


        void AddButtons()
        {
            Image UpgradeImage = GameWorld.Images["UpgradeButtonImage"];
            UpgradeTowerButton UpgradeTowerButton = new UpgradeTowerButton(GameWorld, DisplayRectangle, Position + new Vector2(-MySprite.Width*ScaleFactor/4, (-UpgradeImage.Height * 0.5f)), UpgradeImage, 0.5f, "", TowerUpgradeInfo);
            ButtonsInfo.Add(new TowerButtonInfo(UpgradeImage, UpgradeTowerButton));

            Image SellButtonImage = GameWorld.Images["SellButtonImage"];
            SellButton SellButton = new SellButton(GameWorld, DisplayRectangle, Position + new Vector2(-MySprite.Width * ScaleFactor / 4, (SellButtonImage.Height * 0.5f) / 2), SellButtonImage, 0.5f, "", TowerUpgradeInfo.Tower, TowerUpgradeInfo.Tower.SellMoney);
            ButtonsInfo.Add(new TowerButtonInfo(SellButtonImage, SellButton));
        }

        public override void Update(float pDeltaTime)
        {
            base.Update(pDeltaTime);
            ShouldKillAllButtons();
            ShouldDie();
            
        }

        void ShouldDie()
        {
            if (TowerUpgradeInfo.Tower.IsDead)
            {
                KillAllButtons();
                Die();
            }
        }

        void ShouldKillAllButtons()
        {
            if (ButtonsInfo.Exists(item => item.Button.IsDead) && ButtonsInfo.FindAll(item => item.Button.IsDead).Count < ButtonsInfo.Count ||
                Form1.ClickedOnScreen && !MouseStay)
            {
                TowerUpgradeInfo.Tower.DrawAttackRange = false;
                KillAllButtons();
            }
        }

        void KillAllButtons()
        {
            foreach (TowerButtonInfo ButtonInfo in ButtonsInfo)
            {
                ButtonInfo.Button.Die();
            }
        }


 
        protected override void OnMouseDown()
        {
            TowerUpgradeInfo.Tower.DrawAttackRange = true;
            foreach (TowerButtonInfo ButtonInfo in ButtonsInfo)
            {
                Button ButtonToAdd = ButtonInfo.Button;
                if (ButtonToAdd.IsDead || !GameWorld.Objects.Exists(item => item == ButtonToAdd))
                {
                    ButtonToAdd.Reset();
                    GameWorld.AddObjectToNextCycle(ButtonToAdd);
                }
            }
        }
    }
}
