﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace OOP___Tower_defense
{
    class Explosion : GameObject
    {
        private Circle ExplosionCircle;
        private int ExplosionRadius;
        private float Damage;
        private Vector2 ExplosionCenter;
        private float DeathTimer;
        private List<Enemy> EnemiesDamaged = new List<Enemy>();

        public Explosion(GameWorld gameWorld, Rectangle displayRectangle, Vector2 position, List<Image> images, float scaleFactor, float damage) : 
            base(gameWorld,displayRectangle,position,images,scaleFactor)
        {
            SoundManager.Play(@"Sounds\explosion.wav");
            ExplosionRadius = (int)(25f * ScaleFactor);
            Damage = damage;
            Position -= new Vector2(MySprite.Width * ScaleFactor / 2, MySprite.Height * ScaleFactor / 2);
            ExplosionCenter = GetCenter();
            ExplosionCircle = new Circle(ExplosionCenter, ExplosionRadius);
            AnimationSpeed = 40;
            DeathTimer = images.Count * (1 / AnimationSpeed); // Calculating how long the explosion animation takes.
        }

        public override void Update(float pDeltaTime)
        {
            base.Update(pDeltaTime);
            CheckExplosionCollision();
            CountDown(pDeltaTime);
        }

        void CountDown(float deltaTime)
        {
            DeathTimer -= deltaTime;
            if (DeathTimer <= 0)
            {
                Die();
            }
        }

        public override void Draw(Graphics pGraphics)
        {
            base.Draw(pGraphics);
         /*   Vector2 CenterPosition = Position - new Vector2(ExplosionRadius - CollisionBox.Width / 2, ExplosionRadius - CollisionBox.Height / 2);
            pGraphics.DrawEllipse(new Pen(Color.Red), new RectangleF(CenterPosition.X, CenterPosition.Y, ExplosionRadius*2, ExplosionRadius*2));*/
        }

        void CheckExplosionCollision()
        {
            Enemy EnemyCollision = (Enemy)GameWorld.Objects.Find(item => ExplosionCircle.IntersectsWith(item.CollisionBox) && item is Enemy);
            if (EnemyCollision != null && !EnemiesDamaged.Exists(item => item == EnemyCollision))
            {
                EnemyCollision.Health -= Damage;
                EnemiesDamaged.Add(EnemyCollision);
            }
        }

        public override void OnCollision(GameObject pOtherObject)
        {
            
        }
    }
}
