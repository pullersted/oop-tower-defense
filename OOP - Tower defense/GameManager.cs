﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP___Tower_defense
{
    static class GameManager
    {
        public static bool IsActive;
        public static bool LastWave;

        private static float MyMoney = 75; // 75 Current start
        private static int CurrentLevel = 0;
        private static int MyLife = 20;

        public static float Money
        {
            get
            {
                return MyMoney;
            }
            set
            {
                if(value >= 0)
                {
                    MyMoney = value;
                }
            }
        }

        public static int Life {
            get
            {
                return MyLife;
            }
            set
            {
                if (value >= 0)
                {
                    MyLife = value;
                }
                else if(value < 0)
                {
                    MyLife = 0;
                }
            }
        }

        public static int Level
        {
            get
            {
                return CurrentLevel;
            }
            set
            {
                 CurrentLevel = value;
            }
        }

        private static void DisableGame()
        {
            IsActive = false;
        }

        public static void Lose()
        {
            Lose LoseForm = new Lose();
            Form1 GameForm = Form1.Self;
            GameForm.Hide();
            DisableGame();
            LoseForm.Location = GameForm.Location;
            LoseForm.Show();
        }

        public static void Win()
        {
            Win WinForm = new Win();
            Form1 GameForm = Form1.Self;
            GameForm.Hide();
            DisableGame();
            WinForm.Location = GameForm.Location;
            WinForm.Show();
        }

        
    }
}
